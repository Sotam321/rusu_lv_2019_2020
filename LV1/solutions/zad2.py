num = input("insert number between 0 and 1 : ")

try:
    num = float(num)
except ValueError:
    print("You didn't enter a number!")
    exit()

if (num > 1 or num < 0):
    print("number must be between 0 and 1!")
    exit()
elif (num >= 0.9):
    print("A")
elif (num >= 0.8):
    print("B")
elif (num >= 0.7):
    print("C")
elif (num >= 0.6):
    print("D")
else:
    print("F")

